![Web Preview Image](https://vtolvr-mods.com/media/uploaded_mod_images/web_previewlsAVworQCp.png)

This is the Media Controls mod source code for VTOL VR. You can view the mod [here at vtolvr-mods.com](https://vtolvr-mods.com/mod/vtzgco5u/).

## Dependencies

- Assembly-CSharp.dll
- ModLoader.dll
- UnityEngine.AudioModule.dll
- UnityEngine.CoreModule.dll
- UnityEngine.dll
- 0Harmony.dll
- CSCore.dll

CSCore.dll can be found from [here](https://github.com/filoe/cscore). The rest can be found in your VTOL VR folder.

## Contributions

@jakecraige for adding support for the volume knob to control just the music player in !1 update 2.0.0

@thegreatoverlordofallcheese for adding chrome and edge into the list of supported multi media applications